#pragma once
#ifndef FOLLOWINGAGENT_H
#define FOLLOWINGAGENT_H

#include"Vehicle.h"
#include "LeaderAgent.h"

class FollowingAgent : public Vehicle {
public:
	FollowingAgent(GameWorld* world,
		Vector2D position,
		double    rotation,
		Vector2D velocity,
		double    mass,
		double    max_force,
		double    max_speed,
		double    max_turn_rate,
		double    scale,
		Vehicle*  target,
		Vector2D  offset);
};
#endif // !FOLLOWINGAGENT_H
